from telethon import TelegramClient
from telethon.tl.functions.messages import SendMessageRequest
from telethon.tl.types import InputPeerUser
import asyncio
import nest_asyncio
import telethon.sync
import time
from tqdm import tqdm
nest_asyncio.apply()

api_id = 3245664                               
api_hash = '8fb53df3f58db04ebcd626732dac5452'

client = TelegramClient('Audience', api_id, api_hash)
await client.start()
await client.connect()

chanels = ['ozon_partner', 'sellerozon', 'marketplaysiUFO', 'wbsupplier',
          'helpmarketplaces', 'sellercenter_online', 'gildia_marketplace',
          'ebaydelaydengi', 'Europe5k', 'sberseller', 'sber_mega_market',
          'ali_partner', 'aliexpresspartneri', 'ym_partners', 'yandexmarketpartners',
          'chat_marketplace_YM', 'sellersmessenger', 'wb_ozon_market', 'Ozon_community',
          'naWBchat', 'naWBchat', 'ozon_fbs', 'ozon_fbs', 'wildberries_marketplace_chat',
          'wildberries_marketplace_chat']

all_users = []
for i in tqdm(chanels):
    users = await client.get_participants(i)
    for user in users:
        if {'id':user.id, 'username':'@'+str(user.username), 'phone':user.phone, 'access_hash':user.access_hash} not in all_users:
            all_users.append({'id':user.id, 'username':'@'+str(user.username), 'phone':user.phone,
                          'access_hash':user.access_hash}) 
            
with open('all_users.txt', 'w') as f:
    for i in all_users:
        f.write(str(i['id'])+','+i['username']+','+str(i['phone'])+','+str(i['access_hash'])+'\n')       
